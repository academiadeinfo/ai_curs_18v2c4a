#include <iostream>
#include <conio.h>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <windows.h>

using namespace std;

void poveste()
{
    cout << "Dupa ce si-a terminat temele Elif patrunde prin portalul ascuns din spatele calculatorului in lumea fantastica a Codarniei." <<endl;
    getch();
    cout << "Aici el afla ca Ruby, printesa regatului Rails a fost rapita de capcaunul Pehasp, si este tinuta captiva in turnul cel inalt." <<endl;
    getch();
    cout << "Pentru a ajunge acolo si a se lupta cu capcaunul pentru viata printesei, Elif trebuie sa treaca mai intai prin intunecata padure a gandacilor, plina de gandaci giganti carnivori.";
    cout << "\n\nGata de aventura? Sa pornim !" << endl;
    getch();
}

struct Personaj
{
    string nume;
    string salut, victorie, infrangere;
    string numeFisier;
    int ap = 15, dp = 10, hp = 100, nivel = 0, bani = 10;

    void spuneSalut(){  cout << nume << ": " << salut << endl; }
    void spuneVictorie(){cout << nume << ": " << victorie << endl; }
    void spuneInfrangere(){cout << nume << ": " << infrangere << endl; }

    void load(string nF)
    {
        numeFisier = nF;
        ifstream f(numeFisier.data());
        getline(f, nume);
        f >> nivel >> bani >> hp >> ap >> dp;
        getline(f, salut); ///citim restul liniei de mai sus
        getline(f, salut); /// citim linia de salut
        getline(f, victorie);
        getline(f, infrangere);
        f.close();
    }
    void save()
    {
        ofstream fout(numeFisier.data());
        fout << nume <<endl;
        fout << nivel << " " << bani << " " <<hp << " " << ap << " " << dp << endl;
        fout << salut << endl << victorie << endl << infrangere << endl;
    }
    void createNew()
    {
        cout << "Nume: ";
        cin >> nume;
        ap = 10, dp = 5, hp = 100, nivel = 0, bani = 50;
        salut = "O sa te inving!";
        infrangere = "M-ai invins de data asta, dar ma voi intoarce!";
        victorie = "Ha, te-am invins! inca un pas spre salvarea printesei.";
        cout << "Fisier: ";
        cin >> numeFisier;
        save();
    }

    bool antrenament(int &stat, int points, int cost, int maxVal = 500)
    {
        if(bani >= cost)
        {
            stat = min(maxVal, stat+points);
            bani -= cost;
            return true;
        }
        else
            return false;
    }
};

Personaj player;
int nInamici = 1;
string fisiereInamici[16] = {"inamic1.pers", "inamic2.pers"};
Personaj inamici[16];

void personajStatus(Personaj pers)
{
    cout << pers.nivel << " " << pers.nume << "   " << "hp: " << pers.hp<<endl;
    cout << "ap: " << pers.ap << "  dp: " << pers.dp << "  $" << pers.bani << endl;
}

void batalieStatus(Personaj inamic)
{
    system("cls");
    personajStatus(player);
    cout << "\n\n\tVS\n\n";
    personajStatus(inamic);
    cout << endl;
}

void lovitura(Personaj & atacator, Personaj & aparator, int dirAtac, int dirAparare)
{
    ///regulile de lupta (in functie de ap, dp și directii de atac și aparare)
    if(dirAtac == dirAparare)
    {
        int loss = max(0, atacator.ap - 2*aparator.dp);
        cout << endl << atacator.nume << " ataca dar " << aparator.nume <<
            " reuseste sa se apere si pierde doar " << loss << " hp." << endl;
        aparator.hp -= loss;
    }
    else
    {
        int loss = max(0, atacator.ap - aparator.dp);
        cout << endl << atacator.nume << " ataca pe neasteptate si " << aparator.nume <<
                " pierde " << loss << " hp." << endl;
        aparator.hp -= loss;
    }
}

void ataca(Personaj &inamic)
{
    cout << "Alege directia de atac: (1, 2 sau 3)";
    int dirAtac = getch() - '0';
    int dirAparare = rand()%3 + 1;

    if(dirAtac < 1 || dirAtac > 3)
    {
        cout << "Te-ai impiedicat de sabie si te-ai lovit singur" << endl;
        player.hp -= player.ap / 2;
    }
    else
        lovitura(player, inamic, dirAtac, dirAparare);
}

void apara(Personaj &inamic)
{
    cout << "Alege directia de aparare: (1, 2 sau 3)";
    int dirAparare = getch() - '0';
    int dirAtac = rand()%3 + 1;
    lovitura(inamic, player, dirAtac, dirAparare);
}

void finalBatalie(Personaj inamic)
{
    if(player.hp > 0)
    {
        player.spuneVictorie();
        inamic.spuneInfrangere();
        cout << "-------------\n";
        cout << "Ai castigat " << inamic.bani << " codoni" << endl;
        player.bani += inamic.bani;
        player.nivel++;
    }
    else
    {
        player.spuneInfrangere();
        inamic.spuneVictorie();
        cout << "--------------\n";
        cout << "Te-ai retras, te-ai odihnit si ti-ai refacut 50 hp";
        player.hp = 50;
    }
    getch();
}

void batalie(Personaj inamic)
{
    batalieStatus(inamic);
    cout << "\n\n";
    player.spuneSalut();
    inamic.spuneSalut();
    cout <<"\n\napasa orice tasta pentru a incepe lupta ...";
    getch();
    while(inamic.hp > 0 && player.hp > 0)
    {
        batalieStatus(inamic);
        ataca(inamic);
        apara(inamic);
        cout << "\nApasa orice tasta pentru a continua\n";
        getch();
    }
    finalBatalie(inamic);
}

void meniu()
{
    system("cls");
    personajStatus(player);
    cout << "\n\n";
    cout << "X: antrenament atac (15c)" << endl;
    cout << "C: antrenament aparare (10c)" << endl;
    cout << "H: vindecare viata (10c)" << endl;
    cout << "SPACE: mergi la lupta !" << endl;
    char command = getch();
    if(command == 'x')
        if(player.antrenament(player.ap, 5, 15))
            cout << "te-ai antrenat si ai obtinut 5ap\n";
        else
            cout << "fonduri insuficiente\n";
    else if(command == 'c')
        if(player.antrenament(player.dp, 5, 10))
            cout << "te-ai antrenat si ai obtinut 5dp\n";
        else
            cout << "fonduri insuficiente\n";
    else if(command == 'h')
        if(player.antrenament(player.hp, 100, 15, 100))
            cout << "te-ai odihnit si ai obtinut 100hp\n";
        else
            cout << "fonduri insuficiente\n";
    else if(command == ' ')
        batalie(inamici[min(player.nivel, nInamici)]);///batalie(monstru[player.nivel]);

    player.save();
}

void meniuJoc()
{
    cout << "[N] New Game" << endl;
    cout << "[L] Load Game" << endl;
    char c = getch();
    if(c == 'N' || c == 'n')
        player.createNew();
    else
    {
        cout << "file: ";
        string numeFisier;
        cin >> numeFisier;
        player.load(numeFisier);
    }
}

int main()
{
    meniuJoc();
    if(player.nivel == 0)
    {
        poveste();
        player.nivel++;
        player.save();
    }

    for(int i = 1; i <= nInamici; i++)
        inamici[i].load(fisiereInamici[i]);

    while(1)
    {
        meniu();
    }

    return 0;
}
