#include <iostream>
#include <windows.h>
#include <conio.h>

using namespace std;

struct Ecran
{
    HANDLE consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    void hideCursor()
    {
        CONSOLE_CURSOR_INFO info;
        info.dwSize = 100;
        info.bVisible = FALSE;
        SetConsoleCursorInfo(consoleHandle, &info);
    }
    void gotoxy(int x, int y)
    {
      COORD c = { x, y };
      SetConsoleCursorPosition(consoleHandle,c);
    }
    void print(int x, int y, string c )
    {
        gotoxy(x, y);
        cout << c;
    }
}ecran;

int x = 10, y = 10;

void movexy(char dir)
{
    if(dir == 'w' || dir == 'W' || dir == 72)
        y--;
    else if(dir == 'a' || dir == 'A' || dir == 75)
        x--;
    else if(dir == 'd')
        x++;
    else if(dir == 's')
        y++;
}

int main()
{
    ecran.hideCursor();
    while(1)
    {
        ecran.print(x, y, "O");
        char dir = getch();
        movexy(dir);
    }

    return 0;
}
