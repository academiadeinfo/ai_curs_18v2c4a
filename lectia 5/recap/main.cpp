#include <iostream>
#include <ctime>
#include <conio.h>
#include <cstdlib>
#include <algorithm>

using namespace std;

struct Vopsea
{
    string culoare = "necunoscut";
    int intensitate;

    Vopsea(){}
    Vopsea(string col, int intens)
    {
        culoare = col;
        intensitate = intens;
    }
};

Vopsea operator + (Vopsea a, Vopsea b)
{
    Vopsea rezultat("combinatie", (a.intensitate+b.intensitate) / 2);
    if(a.culoare > b.culoare)
        swap(a, b);
    if(a.culoare == "alb")
        rezultat.culoare = b.culoare + " deschis";
    else if(a.culoare == "galben" && b.culoare == "rosu")
        rezultat.culoare = "portocaliu";
    else if(a.culoare == "albastru" && b.culoare == "rosu")
        rezultat.culoare = "mov";
    else if(a.culoare == "albastru" && b.culoare == "galben")
        rezultat.culoare = "verde";
    else
        rezultat.culoare = "maro";

    return rezultat;
}

istream & operator >> (istream &sin, Vopsea &nou)
{
    sin >> nou.culoare >> nou.intensitate;
}

ostream & operator << (ostream &sout, Vopsea v)
{
    sout << "( " << v.culoare << " " << v.intensitate << ") ";
}

bool operator < (Vopsea a, Vopsea b)
{
    return a.intensitate < b.intensitate;
}

int main()
{

    Vopsea v1, v2;
    cout << "vopsea1: ";
    cin >> v1;
    cout << "vopsea2: ";
    cin >> v2;
    if(v1 < v2)
        cout << v1 << " este mai slaba decat " << v2;
    else
        cout << v2 << " este mai slaba decat " << v1;
    cout << endl << min(v1, v2);
    cout << endl << "combinate dau: " << v1+v2 <<endl<<endl;

    srand(time(NULL));
    string culori_posibile[30] = {"rosu", "galben", "albastru", "verde", "ocru", "alb", "alb"};

    ///generăm 10 vopsele random:
    Vopsea vopsele[32];
    for(int i = 1; i <= 10; i++)
        vopsele[i] = Vopsea(culori_posibile[rand()%7], rand()%24);

    ///le afișăm
    for(int i = 1; i <= 10; i++)
        cout << vopsele[i] << endl;
    cout << endl << endl;

    sort(vopsele+1, vopsele+11); ///sortam sirul de la pozitia 1 la pozitia 11

    ///afisam din nou (sirul sortat de data asta)
    for(int i = 1; i <= 10; i++)
        cout << vopsele[i] << endl;

    return 0;
}
