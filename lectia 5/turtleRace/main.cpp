#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

struct Testoasa{
    int viteza, progres = 0;
    Testoasa(){}
    Testoasa(int vit)
    {
        viteza = vit;
    }

    void afisare()
    {
        for(int i = 1; i <= progres; i++)
            cout << " ";
        cout << ">>"<<endl;
    }

    void avansare()
    {
        progres += viteza;
    }
};

Testoasa concurenti[30];
int nConcurenti = 10;
int vitezaSimulare = 200000;
int lungimeCursa = 64;

void init()
{
    for(int i = 1; i <= nConcurenti; i++)
    {
        concurenti[i].viteza = rand()%3+1;
        concurenti[i].progres = 0;
    }
}

void afisare()
{
    for(int i = 1; i <= nConcurenti; i++)
    {
        concurenti[i].afisare();
        for(int i = 1; i <= lungimeCursa; i++)
            cout << "-";
        cout << endl;
    }
}

void run()
{
    bool finished = false;
    while(!finished)
    {
        system("cls");
        afisare();
        int avans = rand() % nConcurenti+1;
        concurenti[avans].avansare();
        if(concurenti[avans].progres == lungimeCursa)
            finished = true;
        for(int i = 1; i <= vitezaSimulare; i++);
    }
}



int main()
{
    srand(time(NULL));
    init();
    run();
    return 0;
}
