#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <windows.h>
#include <conio.h>

using namespace std;

///obținera unei valori random dintr-un șir de elemente

struct Elev
{
    string nume;
    int puncte;
    Elev(){  }
    Elev(string num, int pct)
    {
        nume = num;
        puncte = pct;
    }
};

Elev elevi[30];
int nElevi;

void citire()
{
    ifstream f ("elevi.data");
    f >> nElevi;
    for(int i = 1; i <= nElevi; i++)
        f >> elevi[i].nume >> elevi[i].puncte;
    f.close();
}

void salvare()
{
    ofstream f ("elevi.data");
    f << nElevi << endl;
    for(int i = 1; i <= nElevi; i++)
        f << elevi[i].nume << "\t" << elevi[i].puncte << endl;
    f.close();
}

void afisare()
{
    for(int i = 1; i <= nElevi; i++)
        cout << elevi[i].nume <<"\t" <<elevi[i].puncte<<endl;
}

int main()
{
    srand(time(NULL));

    citire();

    while(true)
    {
        system("cls");
        afisare();
        cout << "\n\n";
        int ghinionist = rand()%nElevi + 1; ///indice random de la 1 la nElevi
        char raspuns;

        cout << "Sa iasa la tabla: " << elevi[ghinionist].nume << endl;
        cout << "\nA raspuns corect? (D / N): ";
        raspuns = getch();
        if(raspuns == 'd' || raspuns == 'D')
            elevi[ghinionist].puncte+=2;
        else
            elevi[ghinionist].puncte--;

        salvare();
    }

    return 0;
}
