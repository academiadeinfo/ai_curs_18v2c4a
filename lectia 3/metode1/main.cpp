#include <iostream>

using namespace std;

struct Catel
{
    string rasa, nume;
    int varsta;

    void latra()
    {
        if(varsta <= 3)
            cout << nume<< ": HAM!" << endl;
        else
            cout << nume<< ": ROUGH!" << endl;
    }
};

void citire(Catel & catel)
{
    cin >> catel.nume >> catel.rasa >> catel.varsta;
}

int main()
{
    Catel c1, c2;
    citire(c1);
    citire(c2);
    c1.latra();
    c2.latra();

    return 0;
}
