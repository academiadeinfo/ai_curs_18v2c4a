#include <iostream>

using namespace std;

/**
Faceți o structură numită Dreptunghi.
Aceasta va avea câmpurile lungime, lățime, și culoare; si metodele:
citeste(fără parametrii, citește de la tastatură valorile, nu returnează nimic.)
aria(fără parametrii, returneaza aria dreptunghiului)
perimetru (fără parametrii, returnează perimetrul).

Se citesc 2 dreptunghiuri, afișați culoarea celui cu aria maximă (folosind funcțiile).
Să se determine dreptunghiul cu perimetrul minim (dintre cele de la pct a)
*/

struct Dreptunghi
{
    string culoare;
    int lung, lat;

    void citire()
    {
        cin >> lung >> lat >> culoare;
    }
    int aria()
    {
        return lung * lat;
    }
    int perimetru()
    {
        return 2*(lung+lat);
    }
    void afisare()
    {
        for(int i = 1; i <= lat; i++)
        {
            for(int j = 1; j <= lung; j++)
                cout << "* ";
            cout << endl;
        }
    }
};

int main()
{
    Dreptunghi dr1, dr2;

    dr1.citire();
    dr2.citire();
    if(dr1.aria() > dr2.aria())
        cout << dr1.culoare;
    else
        cout << dr2.culoare;
    cout << endl;
    if(dr1.perimetru() > dr2.perimetru())
        dr1.afisare();
    else
        dr2.afisare();

    return 0;
}
