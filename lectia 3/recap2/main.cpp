#include <iostream>
#include <fstream>

using namespace std;

struct Vaca
{
    int id, lapte, mancare;
};

int n;
Vaca v[1024];
int pretMancare, pretLapte;

void citire()
{
    ifstream listaVaci("vaci.txt");
    listaVaci >> n;
    for(int i = 1; i <= n; i++)
        listaVaci >> v[i].id >> v[i].mancare >> v[i].lapte;
    listaVaci.close(); //optional în problema asta (folosesc fișierul o singură dată)
}

int lapteTotal()
{
    int suma = 0;
    for(int i = 1; i <= n; i++)
        suma += v[i].lapte;
    return suma;
}

int mancareTotal()
{
    int suma = 0;
    for(int i = 1; i <= n; i++)
        suma += v[i].mancare;
    return suma;
}

bool profitabila(Vaca vaca)
{
    if(pretMancare * vaca.mancare > pretLapte * vaca.lapte)
        return false;
    else
        return true;
}

int main()
{
    citire();
    cout << "Lapte total pe zi: \t " << lapteTotal() << endl;
    cout << "Mancare consumata pe zi:\t" << mancareTotal() << endl;

    cout << "Pret mancare (/Kg):\t";
    cin >> pretMancare;
    cout << "Pret lapte (/L):\t";
    cin >> pretLapte;
    cout << "Urmatoarele vaci nu sunt profitabile: ";
    for(int i = 1; i <= n ;i ++)
        if(!profitabila(v[i]))
            cout << v[i].id << " ";

    return 0;
}
