#include <iostream>
#include <conio.h>
#include <ctime>
#include <windows.h>

using namespace std;

int main()
{
    char c;
    while(c != ' ' + 1)
    {
        c = getch() + 1; /// citește un caracter fără să îl afișeze în consolă
        cout << c;
    }

    system("cls"); /// curăță ecranul;

    cout << time(NULL) << endl;

    srand(time(NULL)); ///setează seed-ul pentru numere random
    for(int i = 1; i <= 5; i++)
        cout << rand() % 20<< " "; /// ne dă un număr random între 0 și 20


    return 0;
}
