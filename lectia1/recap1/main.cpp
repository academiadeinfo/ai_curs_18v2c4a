#include <iostream>

using namespace std;

/**
Scrieți funcția maxim de tipul int care primește ca parametrii 2
numere întregi și returnează maximul dintre ele.
În main, folosiți funcția maxim pentru a calcula maximul dintre 3 numere.

*/
int maxim(int a, int b)
{
    if (a>b)
        return a;
    else
        return b;
}


int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    cout << maxim(maxim(a,b),c);
    return 0;
}
