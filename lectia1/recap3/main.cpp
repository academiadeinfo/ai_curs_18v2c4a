#include <iostream>

using namespace std;

/**
Se citesc trei numere naturale a și b și c.
    Să se interschimbe a și b.
    Să se interschimbe b și c.
    Să se interschimbe a și c
-> folosiți o funcție de tip void care interschimbă 2 numere
*/

void inter(int & x, int & y)
{
    int aux=x;
    x=y;
    y=aux;
}

int main()
{
    int a, b, c;
    cin >> a >> b >> c;
    inter(a,b);
    inter(b,c);
    inter(a,c);

    cout << a << " " << b << " " << c;
    return 0;
}
