#include <iostream>
#include <fstream>
#include <ctime>
#include <conio.h>
#include <windows.h>

using namespace std;

void printAsc(char fisier[])
{
    /// afișăm tot conținutul fișierului fisier.
    char c;
    ifstream f(fisier);
    while( f >> noskipws >> c )
    {
        cout << c;
    }
    f.close();
}

int scorJucator, scorComputer;

int determinaCastigator(int player, int comp)
{
    if(player == comp)
        return 0; /// 0 pentru egalitate
    if(player == 0 && comp == 1)
        return 1; /// 1 pentru câștigă playerul
    if(player == 1 && comp == 2)
        return 1;
    if(player == 2 && comp == 0)
        return 1;
    return 2;    /// 2 pentru castiga computerul
}

int getCode(char c) ///transformă caracterul în codul său.
{
    if(c == 'p')
        return 0;
    else if(c == 'f')
        return 1;
    else if(c == 'h')
        return 2;
    return -1;
}

void printHand(int code)
{
    if(code == 0)
        printAsc("rock.asc");
    else if(code == 1)
        printAsc("scissors.asc");
    else if(code == 2)
        printAsc("paper.asc");
}

int runda()///functie runda() de tip int care returnează 0 pentru egalitate, 1 dacă câștigă playerul, 2 dacă câștigă computerul
{
    int playerCode, compCode;
    char playerChar;
    cout << "Jucator:  " << scorJucator<<endl;
    cout << "Computer: " << scorComputer<<endl<<endl;
    cout << "Alege P - Piatra, F - foarfeca, H - hartie\n\n";  ///cerem utilizatorului sa introduca un cracter: P / F / H
    playerChar = getch();
    playerCode = getCode(playerChar);
    compCode = rand()%3;
    cout << "PLAYER: " << endl;
    printHand(playerCode);
    cout << endl << endl << "COMPUTER: " << endl;
    printHand(compCode);
    cout << endl << endl;
    return determinaCastigator(playerCode, compCode);
}

void updateScore(int castigator)///functie updateScore care are ca parametru 0, 1 sau 2 (câștigătorul),
{
    ///afișează mesajul câștigătorului
    ///updatează scorul.
    if(castigator == 0)
        cout << "Egalitate" << endl;
    else if(castigator == 1)
    {
        cout << "Bravo, ai castigat" << endl;
        scorJucator++;
    }
    else
    {
        cout << "Oups, ai pierdut" << endl;
        scorComputer++;
    }

}

int main()
{

    while(true)
    {
        system("cls");
        updateScore(runda());
        cout << endl << "Apasa orice tasta pentru a continua ...";
        getch();
    }

    return 0;
}
