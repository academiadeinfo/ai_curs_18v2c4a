#include <iostream>

using namespace std;
/**
Faceți o structură “masina” care va avea câmpurile marca, culoare, nrMatr
(numărul de înmatriculare) și anul fabricației.

Se citesc datele a două mașini, care este mai nouă (nr de inmatriculare)?
Este vreuna de culoarea "negru"?

*/

struct Masina
{
    string marca, culoare, nrMatr;
    int anFabricatie;
};

void citire(Masina & m)
{
    cout << "nrMatr: ";
    cin >> m.nrMatr;
    cout << "Marca: ";
    cin >> m.marca;
    cout << "Culoare: ";
    cin >> m.culoare;
    cout << "An fabricatie: ";
    cin >> m.anFabricatie;
    cout << endl;
}

int main()
{
    Masina a, b;
    cout << "Masina 1: " << endl;
    citire(a);
    cout << "Masina 2: " << endl;
    citire(b);

    if(a.anFabricatie > b.anFabricatie)
        cout << "mai noua: " << a.nrMatr << endl;
    else if(b.anFabricatie > a.anFabricatie)
        cout << "mai noua: " << b.nrMatr << endl;
    else
        cout << "La fel de vechi: " << a.nrMatr << " " << b.nrMatr << endl;

    if(a.culoare == "negru" || b.culoare == "negru")
        cout << "Exista o masina neagra" << endl;
    else
        cout << "Nu avem masini negre" << endl;

    return 0;
}
