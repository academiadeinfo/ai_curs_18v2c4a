#include <iostream>

using namespace std;

struct fractie
{
    int sus, jos;
};

void afisare(fractie f)
{
    cout << "( " << f.sus << " / " << f.jos << " )";
}

int main()
{
    fractie a, b, sir[100];

    a.sus = 5;
    a.jos = 7;

    b.sus = 9;
    b.jos = 3;

    afisare(a);
    cout << endl;
    afisare(b);


    return 0;
}
