#include <iostream>

using namespace std;

struct fractie
{
    int sus, jos;
};

void citire(fractie & f)
{
    cin >> f.sus >> f.jos;
}

fractie maxim(fractie a, fractie b)
{
    if(a.sus * b.jos > b.sus * a.jos)
        return a;
    else
        return b;
}

void afisare(fractie f)
{
    cout << "( " << f.sus << " / " << f.jos << " )";
}

int main()
{
    fractie x, y;
    citire(x);
    citire(y);
    afisare(maxim(x, y));

    return 0;
}
