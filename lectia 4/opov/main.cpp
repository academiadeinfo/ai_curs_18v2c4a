#include <iostream>
#include <fstream>

using namespace std;

struct Elev
{
    string nume, prenume;
    int nrNote = 0;
    int nota[128];

    double media()
    {
        double suma = 0;
        for(int i = 1; i <= nrNote; i++)
            suma += nota[i];
        return suma / nrNote;
    }
};

Elev max(Elev a, Elev b)
{
    if(a.media() > b.media())
        return a;
    return b;
}

istream & operator >> (istream &fin, Elev & elev)
{
    fin >> elev.nume >> elev.prenume >> elev.nrNote;
    for(int i = 1; i <= elev.nrNote; i++)
        fin >> elev.nota[i];
}

Elev operator + (Elev elev, int nota)
{
    Elev nou = elev;
    nou.nota[++nou.nrNote] = nota;
    return nou;
}

int main()
{
    ifstream fisierElevi ("elevi.in");
    int nElevi;
    Elev e1, e2;

    fisierElevi >> nElevi;

    fisierElevi >> e1;

    cout << e1.media() << endl;
    cout << (e1 + 10).media() << endl;

    return 0;
}
