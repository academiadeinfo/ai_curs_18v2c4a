#include <iostream>

using namespace std;

/**
Un Laptop are o marcă, o memorie RAM și o frecvență de procesor (double).
Creati 3 constructori:
Primul, fără parametrii, va crea un laptop cu marca "unknown", 4 ram și 2.2 procesor
Al doilea, cu un parametru int x și unul double y va crea un laptop cu marca "unknown",
            x ram și y frecventa procesor
Al 3-lea, cu un string un int si un double, care construieste un laptop cu toate datele.

În main creati 3 laptopuri folosind cei 3 constructori diferiti.

Care este marca laptopului cu cel mai mult RAM?
Dar seria laptopului cu procesorul cel mai rapid?
*/

struct Laptop
{
    string marca;
    int ram;
    double ghz;
    Laptop()
    {
        marca = "unknown"; ram = 4; ghz = 2.2;
    }
    Laptop(int r, double hz)
    {
        marca = "unknown"; ram = r; ghz = hz;
    }
    Laptop(string ma, int r, double hz)
    {
        marca = ma; ram = r; ghz = hz;
    }

    void afisare()
    {
        cout << marca << " " << ram <<" Gb RAM, " << ghz << " GHz Procesor" << endl;
    }
};

Laptop maxRam(Laptop a, Laptop b)
{
    if(a.ram > b.ram)
        return a;
    else
        return b;
}

Laptop maxProcesor(Laptop a, Laptop b)
{
    if(a.ghz > b.ghz)
        return a;
    else
        return b;
}

int main()
{
    Laptop a, b(3, 3.2), c("Lenovo", 8, 2.8);
    maxRam(maxRam(a, b), c).afisare();
    maxProcesor(maxProcesor(a, b), c).afisare();

    return 0;
}
