#include <iostream>

using namespace std;

struct Pisica
{
    string nume = "Joahanna Doe", culoare;
    int lene;

    Pisica(string nm, string col, int len)
    {
        nume = nm;
        culoare = col;
        lene = len;
    }
    Pisica()
    {

    }
    void vorbeste()
    {
        cout << nume << ": Mi";
        for(int i = 1; i <= lene; i++)
            cout << "a";
        cout << "u" <<endl;
    }
};


int main()
{
    Pisica p1, p2;

    cin >> p1.nume >> p1.culoare >> p1.lene;
    cin >> p2.nume >> p2.culoare >> p2.lene;

    if(p1.lene < p2.lene)
    {
        p1.vorbeste();
        p2.vorbeste();
    }
    else
    {
        p2.vorbeste();
        p1.vorbeste();
    }

    Pisica p3("Sammy", "maro", 7);
    p3.vorbeste();


    return 0;
}
